const functions = require("./functions");

beforeAll(() => {
    console.log("Initializing....")
})
afterAll(() => {
    console.log("Closing...")
})

test("Check value from outer", () => {
    console.log("Check from outer")
    expect(functions.checkValue(undefined)).toBeUndefined()
})

describe("Describe", () => {
    beforeAll(() => {
        console.log("Initializing inside describe....")
    })
    afterAll(() => {
        console.log("Closing inside describe...")
    })

    test("Check value from inside describe", () => {
        console.log("Check from inside")
        expect(functions.checkValue(undefined)).toBeUndefined()
    })
})