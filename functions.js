const { default: axios } = require("axios");

const functions = {
    add: (num1, num2) => num1 + num2,
    isNull: () => null,
    checkValue: x => x,
    profile: () => {
        return {
            username: "Eka",
        }
    },
    fetchData: async () => {
        const res = await axios.get("https://jsonplaceholder.typicode.com/todos/1");
        return await res.data;
    },
    fetchDataWithError: async () => {
        const res = await axios.get("https://jsonplaceholder.typicode.com/todosd/1");
        return await res.data;
    }
}

module.exports = functions;