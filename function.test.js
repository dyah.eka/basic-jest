const functions = require("./functions");

// beforeEach(() => {
//     console.log("Initializing....")
// })
// afterEach(() => {
//     console.log("Closing...")
// })

beforeAll(() => {
    console.log("Initializing....")
})
afterAll(() => {
    console.log("Closing...")
})


// toBe
test("Adds 2 + 2 equals 4", () => {
    console.log("tobe..")
	expect(functions.add(2, 2)).toBe(4);
});
test("Adds 2 + 2 Not equals 5", () => {
	expect(functions.add(2, 2)).not.toBe(5);
});
test("1. Should be null", () => {
	expect(functions.isNull()).toBe(null);
});

// Falsy and Truthy
test("Substract function undefined", () => {
    console.log("falsy and truthy..")
	expect(functions.substract).toBeUndefined();
});
test("Add function defined", () => {
	expect(functions.add).toBeDefined();
});
test("2. Should be null", () => {
	expect(functions.isNull()).toBeNull();
});
test("Should not be falsy", () => {
	expect(functions.checkValue(0)).toBeFalsy();
});
test("Should not be truthy", () => {
	expect(functions.checkValue(2)).toBeTruthy();
});

// Numbers
test("number 1 plus number 2 less than 16", () => {
    console.log("number..")
	const number1 = 7,
		number2 = 8;
	expect(number1 + number2).toBeLessThan(16);
});
test("number 1 multiply by number 2 equal or greater than 16", () => {
	const number1 = 2,
		number2 = 8;
	expect(number1 * number2).toBeGreaterThanOrEqual(16);
});

// Floating points
test("number 1 multiply by number 2 equal to 0.02", () => {
    console.log("floating points..")
	const number1 = 0.2,
		number2 = 0.1;
	expect(number1 * number2).toBeCloseTo(0.02);
});
test("number 1 multiply by number 2 NOT equal to 0.02", () => {
	const number1 = 0.3,
		number2 = 0.1;
	expect(number1 * number2).not.toBeCloseTo(0.02);
});

// Strings
test("email must be contains @ character", () => {
    console.log("strings..")
	const email = "eka@eka.com";
	expect(email).toMatch(/@/);
});

// Array and Object
test("Username equals Eka", () => {
    console.log("Array and Object..")
	const profile = functions.profile();
	expect(profile).toEqual({
		username: "Eka",
	});
});
test("Contain fire", () => {
	const elements = ["water", "fire", "earth", "dark", "light"];
	expect(elements).toContain("fire");
});
test("NOT contain wind", () => {
	const elements = ["water", "fire", "earth", "dark", "light"];
	expect(elements).not.toContain("wind");
});
test("Array contain 1 and 2 only", () => {
	const arr = [1];
	arr.push(2);
	expect(arr).toEqual([1, 2]);
});

// Async
test("The userId equals to 1", async () => {
    console.log("Async..")
    expect.assertions(1);
	const data = await functions.fetchData();
	expect(data.userId).toBe(1);
});
test("Request should be failed", async () => {
	expect.assertions(1);
	try {
		await functions.fetchDataWithError();
	} catch (error) {
        expect(error.message).toMatch('Request failed with status code 404')
    }
});
